def handler(context, inputs):
    greeting = "Hello test of COMMIT COMPARE AGAIN ITS ME, {0}!".format(inputs["target"])
    print(greeting)

    outputs = {
      "greeting": greeting
    }

    return outputs
